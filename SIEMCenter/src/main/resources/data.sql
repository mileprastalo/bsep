/* Insert Privileges */
INSERT INTO privilege (id, name) VALUES ('101', 'CREATE_OPERATOR');
INSERT INTO privilege (id, name) VALUES ('102', 'REQUEST_SSL_CERTIFICATE');
INSERT INTO privilege (id, name) VALUES ('103', 'READ_LOGS');
INSERT INTO privilege (id, name) VALUES ('109', 'READ_REPORTS');
INSERT INTO privilege (id, name) VALUES ('110', 'READ_ALARMS');
/* Insert Roles */
INSERT INTO role (id, name) VALUES ('104', 'ADMIN');
INSERT INTO role (id, name) VALUES ('105', 'OPERATOR');

/* Add Privileges to Admins */
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('104', '101');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('104', '102');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('104', '103');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('104', '109');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('104', '110');

/* Add Privileges to Operators */
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('105', '103');
INSERT INTO roles_privileges (role_id, privilege_id) VALUES ('105', '110');

/* Insert Users */
INSERT INTO user (id, password, username) VALUES (106, '$2a$10$PxvKsblywCsPRpmccc2Id.Vf5bMDvXfRMxUhmhhAL1gxXLcWDAhIa', 'admin'); 
INSERT INTO user (id, password, username) VALUES (107, '$2a$10$PxvKsblywCsPRpmccc2Id.Vf5bMDvXfRMxUhmhhAL1gxXLcWDAhIa', 'op1'); 
INSERT INTO user (id, password, username) VALUES (108, '$2a$10$PxvKsblywCsPRpmccc2Id.Vf5bMDvXfRMxUhmhhAL1gxXLcWDAhIa', 'op2'); 

/* Add Roles to Users */
INSERT INTO users_roles (user_id, role_id) VALUES (106, 104);
INSERT INTO users_roles (user_id, role_id) VALUES (107, 105);
INSERT INTO users_roles (user_id, role_id) VALUES (108, 105);
