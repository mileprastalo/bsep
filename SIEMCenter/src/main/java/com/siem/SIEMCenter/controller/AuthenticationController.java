package com.siem.SIEMCenter.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.siem.SIEMCenter.dto.JwtResponseDTO;
import com.siem.SIEMCenter.dto.LoginDTO;
import com.siem.SIEMCenter.service.AuthenticationService;

@RestController
@RequestMapping(path = "/auth", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticationController {

	private AuthenticationService authService;

	public AuthenticationController(AuthenticationService authService) {
		this.authService = authService;
	}

	@PostMapping(path = "/login")
	@CrossOrigin
	public ResponseEntity<JwtResponseDTO> logIn(@RequestBody @Valid LoginDTO authenticationRequest) {
		String jwt = authService.logIn(authenticationRequest);
		JwtResponseDTO dto = new JwtResponseDTO(jwt);
		return new ResponseEntity<JwtResponseDTO>(dto, HttpStatus.OK);
	}
}
