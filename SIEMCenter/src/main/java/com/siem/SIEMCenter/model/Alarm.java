package com.siem.SIEMCenter.model;

import java.util.Date;

import javax.persistence.Id;

import org.kie.api.definition.type.Expires;
import org.kie.api.definition.type.Role;
import org.springframework.data.mongodb.core.mapping.Document;

@Role(Role.Type.EVENT)
@Expires("2m")
@Document
public class Alarm {
	@Id
	private String id;
	private String message;
	private String appName;
	private String service;
	private String computerName;
	private String user;
	private Date creationDate;
	private String system;
	
	public Alarm() {
		// TODO Auto-generated constructor stub
	}

	public Alarm(String message, String appName, String service, String computerName, String user, Date creationDate, String system) {
		super();
		this.message = message;
		this.appName = appName;
		this.service = service;
		this.computerName = computerName;
		this.user = user;
		this.creationDate = creationDate;
		this.system = system;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getComputerName() {
		return computerName;
	}

	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Alarm [message=" + message + ", appName=" + appName + ", service=" + service + ", computerName="
				+ computerName + ", user=" + user  + "]";
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}
	
	
	
}
