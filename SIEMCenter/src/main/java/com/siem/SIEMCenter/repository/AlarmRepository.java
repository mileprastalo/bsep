package com.siem.SIEMCenter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.siem.SIEMCenter.model.Alarm;

public interface AlarmRepository extends MongoRepository<Alarm, String>{
	
	public List<Alarm> findAllOrderByCreationDate();

}
