package com.siem.SIEMCenter.dto;

import java.util.Date;

import com.siem.SIEMCenter.model.Log;

public class LogOutputDTO {

	private String level;
	private Date dateRecorded;
	private String appName;
	private String service;
	private String computerName;
	private String user;
	private String system;
	private String description;

	public LogOutputDTO() {
	}

	public LogOutputDTO(String level, Date dateRecorded, String appName, String service, String computerName,
			String user, String system, String description) {
		this.level = level;
		this.dateRecorded = dateRecorded;
		this.appName = appName;
		this.service = service;
		this.computerName = computerName;
		this.user = user;
		this.system = system;
		this.description = description;
	}

	public LogOutputDTO(Log log) {
		this.level = log.getLevel();
		this.dateRecorded = log.getDateRecorded();
		this.appName = log.getAppName();
		this.service = log.getService();
		this.computerName = log.getComputerName();
		this.user = log.getUser();
		this.system = log.getSystem();
		this.description = log.getDescription();
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Date getDateRecorded() {
		return dateRecorded;
	}

	public void setDateRecorded(Date dateRecorded) {
		this.dateRecorded = dateRecorded;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getComputerName() {
		return computerName;
	}

	public void setComputerName(String computerName) {
		this.computerName = computerName;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
