package com.siem.SIEMCenter.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.HashedMap;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.siem.SIEMCenter.dto.ReportDTO;
import com.siem.SIEMCenter.dto.ReportRequestDTO;
import com.siem.SIEMCenter.model.Alarm;
import com.siem.SIEMCenter.repository.AlarmRepository;

@Service
public class AlarmService {
	
	private AlarmRepository repository;
	
	public AlarmService(AlarmRepository repository) {
		this.repository = repository;
	}
	
	public Alarm save(Alarm alarm) {
		return repository.save(alarm);
	}
	
	
	public ReportDTO report(ReportRequestDTO dto) {
		List<Alarm> alarms = repository.findAll();
		List<Alarm> filteredAlarms = alarms.stream().filter(
				a -> a.getCreationDate().after(dto.getStartDate()) && a.getCreationDate().before(dto.getEndDate()))
				.collect(Collectors.toList());
		ReportDTO report = null;
		if (dto.getParameter().contentEquals("computerName")) {
			report = reportByComputerName(filteredAlarms);
		}
		else {
			report = reportBySystem(filteredAlarms);
		}
		return report;
	}

	private ReportDTO reportByComputerName(List<Alarm> filteredAlarms) {
		Map<String, Integer> report_map = new HashedMap<String, Integer>();
		for (Alarm alarm : filteredAlarms) {
			if (report_map.containsKey(alarm.getComputerName())) {
				int numOfLogs = report_map.get(alarm.getComputerName());
				report_map.put(alarm.getComputerName(), numOfLogs + 1);
			} else {
				report_map.put(alarm.getComputerName(), 1);
			}
		}
		return extractDataFromMap(report_map);
	}
	
	private ReportDTO reportBySystem(List<Alarm> filteredAlarms) {
		Map<String, Integer> report_map = new HashedMap<String, Integer>();
		for (Alarm alarm : filteredAlarms) {
			if (report_map.containsKey(alarm.getSystem())) {
				int numOfLogs = report_map.get(alarm.getSystem());
				report_map.put(alarm.getSystem(), numOfLogs + 1);
			} else {
				report_map.put(alarm.getSystem(), 1);
			}
		}
		return extractDataFromMap(report_map);
	}
	
	private ReportDTO extractDataFromMap(Map<String, Integer> report_map) {
		List<String> labels = new ArrayList<String>();
		List<Integer> data = new ArrayList<Integer>();
		for (Map.Entry<String, Integer> entry : report_map.entrySet()) {
			labels.add(entry.getKey());
			data.add(entry.getValue());
		}
		return new ReportDTO(labels, data);
	}
	public List<Alarm> getAlarms(){
		List<String> order = new ArrayList<String>();
		order.add("creationDate");
		List<Alarm> alarms =  repository.findAll(Sort.by(Sort.Direction.DESC,"creationDate"));
		return alarms;
	}
	

}
