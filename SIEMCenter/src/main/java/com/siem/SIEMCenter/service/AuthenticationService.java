package com.siem.SIEMCenter.service;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.siem.SIEMCenter.config.JwtTokenUtil;
import com.siem.SIEMCenter.dto.LoginDTO;

@Service
public class AuthenticationService {

	private AuthenticationManager authenticationManager;
	private UserDetailsService userService;
	private JwtTokenUtil jwtTokenUtil;

	public AuthenticationService(AuthenticationManager authenticationManager, UserService userService,
			JwtTokenUtil jwtTokenUtil) {
		this.authenticationManager = authenticationManager;
		this.userService = userService;
		this.jwtTokenUtil = jwtTokenUtil;
	}

	public String logIn(LoginDTO authenticationRequest) {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					authenticationRequest.getUsername(), authenticationRequest.getPassword()));
		} catch (BadCredentialsException | InternalAuthenticationServiceException e) {
			throw new BadCredentialsException(authenticationRequest.getUsername());
		}
		final UserDetails userDetails = userService.loadUserByUsername(authenticationRequest.getUsername());
		return jwtTokenUtil.generateToken(userDetails);
	}

}
