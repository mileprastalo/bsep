package com.siem.SIEMCenter.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.siem.SIEMCenter.service.UserService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private JwtAuthenticationEntryPoint restAuthenticationEntryPoint;
	private JwtRequestFilter jwtRequestFilter;
	private UserDetailsService userService;

	public WebSecurityConfig(JwtAuthenticationEntryPoint restAuthenticationEntryPoint,
			JwtRequestFilter jwtRequestFilter, UserService userService) {
		this.restAuthenticationEntryPoint = restAuthenticationEntryPoint;
		this.jwtRequestFilter = jwtRequestFilter;
		this.userService = userService;
	}

	@Bean
	public GrantedAuthorityDefaults grantedAuthorityDefaults() {
		return new GrantedAuthorityDefaults("");
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				// comunication between client and server is stateless
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

				// for unauthorized request send 401 error
				.exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint).and()

				// don't authenticate this particular request
				.authorizeRequests().antMatchers("/auth/**").permitAll()
				.antMatchers(HttpMethod.OPTIONS, "/api/**").permitAll()
				.antMatchers(HttpMethod.POST, "/api/center/save").permitAll()
				.antMatchers(HttpMethod.POST, "/api/alarms/reports").hasAuthority("READ_REPORTS")
				.antMatchers(HttpMethod.GET, "/api/alarms").hasAuthority("READ_ALARMS")
				.antMatchers(HttpMethod.POST, "/api/center/reports").hasAuthority("READ_REPORTS")
				.antMatchers(HttpMethod.POST, "/api/center/request-certificate").hasAuthority("REQUEST_SSL_CERTIFICATE")
				.antMatchers(HttpMethod.POST, "/api/search/**").hasAuthority("READ_LOGS")
				// all other requests need to be authenticated
				.anyRequest().authenticated().and();

		// intercept every request and add filter
		http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

		http.csrf().disable();
		http.requiresChannel().anyRequest().requiresSecure();
	}

}
