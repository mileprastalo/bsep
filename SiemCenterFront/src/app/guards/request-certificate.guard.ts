import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestCertificateGuard implements CanActivate {
  constructor(private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const role = JSON.parse(localStorage.getItem('role'));
      let hasAuthority = false;
      role.some(el => {
        console.log(el.authority);
        if (el.authority === 'REQUEST_SSL_CERTIFICATE') {
          hasAuthority =  true;
          return;
        }
      });
      if (hasAuthority) {
        return true;
      }

      this.router.navigateByUrl('/');
      return false;
  }
  
}
