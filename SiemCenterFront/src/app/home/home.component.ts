import { Component, OnInit, ViewChild } from '@angular/core';
import { Log } from '../model/Log';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }
  logs = new MatTableDataSource<Log>();
  displayedColumns: string[] = ['level', 'dateRecorded', 'service', 'computerName', 'user', 'system', 'description'];
  pageIndex: number = 1;
  length: number = 0;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ngOnInit(): void {
    this.logs.paginator = this.paginator;
  }

  showSearchResult($event) {
    this.logs.data = $event;
    this.length = this.logs.data.length;
    this.logs.paginator = this.paginator;
  }
  pageChange($event) {
    console.log($event);
  }
  reports() {
    window.open("reports");
  }
  alarms() {
    window.open("alarms");
  }
  logOut() {
    localStorage.setItem('token', '');
    this.router.navigateByUrl('/login');
  }
  generateCertificate() {
    window.open("generate-certificate");
  }
}
