export class Log {
    public level: string;
    public dateRecorded: Date;
    public appName: string;
    public service: string;
    public computerName: string;
    public user: string;
    public system: string;
    public description: string;
}
