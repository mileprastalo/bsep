export class ReportDto {

    labels: Array<string>;
    data: Array<number>;

}
