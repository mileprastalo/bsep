export class SearchDto {
    public level: string;
    public date: string;
    public appName: string;
    public service: string;
    public computerName: string;
    public user: string;
    public system: string;
}
