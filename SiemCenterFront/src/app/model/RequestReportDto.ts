export class RequestReportDto {

    public startDate: Date;
    public endDate: Date;
    public parameter: string;
    public typeOfDate: string;

    constructor(startDate: Date, endDate: Date, parameter: string, typeOfDate: string) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.parameter = parameter;
        this.typeOfDate = typeOfDate;
    }
}
