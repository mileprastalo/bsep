package com.SiemAgent.SiemAgent.model;

public class Log {
	private String text;

	public Log() {
	}

	public Log(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
