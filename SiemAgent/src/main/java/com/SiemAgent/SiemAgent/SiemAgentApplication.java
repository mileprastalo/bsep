package com.SiemAgent.SiemAgent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SiemAgentApplication {
	
	
	public static void main(String[] args) {
		SpringApplication.run(SiemAgentApplication.class, args);
	}

}
