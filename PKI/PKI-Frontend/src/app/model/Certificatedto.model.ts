export class Certificatedto {

    id: number;
    commonName: string;
    serialNumber: string;
    signatureAlgorithm: string;
    issuerId: number;
    ca: boolean;
    validFrom: Date;
    validTo: Date;
    childCertificates: Certificatedto[];
    revoked: boolean;
    organisation: string;
    organisationUnit: string;
    country: string;
    issuerName: string;

}
