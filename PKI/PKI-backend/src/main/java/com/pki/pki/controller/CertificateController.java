package com.pki.pki.controller;

import java.io.FileInputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.validation.Valid;

import org.bouncycastle.crypto.tls.CertificateRequest;
import org.bouncycastle.jcajce.provider.asymmetric.RSA;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pki.pki.dto.CertificateDTO;
import com.pki.pki.dto.CertificateRequestDTO;
import com.pki.pki.dto.CertificateRevokedDTO;
import com.pki.pki.dto.IssuerDataDTO;
import com.pki.pki.dto.NewCertificateDTO;
import com.pki.pki.model.Certificate;
import com.pki.pki.service.CertificateService;

@RestController
@RequestMapping(path = "api/certificate")
@CrossOrigin()
public class CertificateController {

	private CertificateService certificateService;

	public CertificateController(CertificateService certificateService) {
		this.certificateService = certificateService;
	}

	@GetMapping(path = "get-cetrificate/{id}")
	public ResponseEntity<CertificateDTO> getCertificate(@PathVariable("id") Long id) {
		Certificate c = certificateService.findById(id);
		CertificateDTO dto = new CertificateDTO(c);
		List<Certificate> children = certificateService.findChildrenCertificates(c);
		List<CertificateDTO> childrenDTO = new ArrayList<>();
		for (Certificate certificate : children) {
			childrenDTO.add(new CertificateDTO(certificate));
		}
		dto.setChildCertificates(childrenDTO);
		return new ResponseEntity<CertificateDTO>(dto, HttpStatus.OK);
	}

	@GetMapping("root-certificate")
	public ResponseEntity<CertificateDTO> getRootCertificate() {
		List<Certificate> roots = certificateService.findRoots();
		if (!roots.isEmpty()) {
			Certificate c = roots.get(0);
			CertificateDTO dto = new CertificateDTO(c);
			List<Certificate> children = certificateService.findChildrenCertificates(c);
			List<CertificateDTO> childrenDTO = new ArrayList<CertificateDTO>();
			for (Certificate certificate : children) {
				childrenDTO.add(new CertificateDTO(certificate));
			}
			dto.setChildCertificates(childrenDTO);
			return new ResponseEntity<CertificateDTO>(dto, HttpStatus.OK);
		}
		return null;
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<CertificateRevokedDTO> revokeCertificate(@PathVariable("id") Long id) {
		this.certificateService.revokeCertificate(id);

		return new ResponseEntity<>(new CertificateRevokedDTO(true), HttpStatus.OK);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> createSelfSignedCertificate(@RequestBody @Valid NewCertificateDTO subjectDTO) {
		boolean result = certificateService.generateSelfSignedCertificate(subjectDTO);
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}

	@PostMapping(value = "/issued", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> createIssuedCertificate(@RequestBody @Valid NewCertificateDTO subjectDTO) {
		boolean result = certificateService.generateIssuedCertificate(subjectDTO);
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}

	@PostMapping(value = "/get-issuer-data")
	public ResponseEntity<IssuerDataDTO> getIssuerData(@RequestBody Long issuerId) {
		IssuerDataDTO data = certificateService.getIssuerData(issuerId);
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
	
	@PostMapping(value = "request", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getCertificateRequest(@RequestBody CertificateRequestDTO dto){
		
		String output = certificateService.certificateRequest(dto);
		return new ResponseEntity<String>(output, HttpStatus.OK);
		
	}

}
