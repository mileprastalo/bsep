package com.pki.pki.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

public class IssuerDataDTO {

	private String organisation;
	private String organisationUnit;
	private String country;
	private String commonName;
	private String start;
	private String end;

	
	public IssuerDataDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public IssuerDataDTO(String organisation, String organisationUnit, String country, String commonName, Date start,
			Date end) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.");
		this.organisation = organisation;
		this.organisationUnit = organisationUnit;
		this.country = country;
		this.commonName = commonName;
		this.start = sdf.format(start);
		this.end = sdf.format(end);
	}





	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getOrganisationUnit() {
		return organisationUnit;
	}

	public void setOrganisationUnit(String organisationUnit) {
		this.organisationUnit = organisationUnit;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	
	
}
