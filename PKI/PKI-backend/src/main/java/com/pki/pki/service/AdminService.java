package com.pki.pki.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.pki.pki.model.Admin;
import com.pki.pki.repository.AdminRepository;

@Service
public class AdminService {
	private final AdminRepository adminRepository;

	public AdminService(AdminRepository adminRepository) {
		this.adminRepository = adminRepository;
	}

	public Admin findById(Long id) {
		return adminRepository.getOne(id);
	}

	public Optional<Admin> findByUsername(String username) {
		return adminRepository.findByUsername(username);
	}
}
