package com.pki.pki.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pki.pki.model.Privilege;

public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {
	Optional<Privilege> findByName(String name);
}
