package com.pki.pki.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pki.pki.service.KeyStoreService;

@RestController
@RequestMapping(path = "api/key-store")
@CrossOrigin()
public class KeyStoreController {
	private KeyStoreService keyStoreService;

	public KeyStoreController(KeyStoreService keyStoreService) {
		this.keyStoreService = keyStoreService;
	}

	@GetMapping
	public ResponseEntity<Boolean> createKeyStore() {
		keyStoreService.createKeyStore();
		return new ResponseEntity<>(true, HttpStatus.CREATED);
	}
}
