package com.pki.pki.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Certificate {

	@Id
	@GeneratedValue
	private Long id;
	@Column
	private String commonName;
	@Column(unique = false, nullable = false)
	private String serialNumber;
	@Column(unique = false, nullable = false)
	private String organisation;
	@Column(unique = true, nullable = false)
	private String organisationUnit;
	@Column(unique = false, nullable = false)
	private String country;
	@Column(unique = false, nullable = false)
	private String signatureAlgorithm;
	@ManyToOne(cascade = { CascadeType.ALL })
	private Certificate issuer;
	@Column(unique = false, nullable = false)
	private boolean isCA;
	@Column(unique = false, nullable = false)
	private Date validFrom;
	@Column(unique = false, nullable = false)
	private Date validTo;
	@Column(unique = false, nullable = false)
	private int issuedCertCounter;
	@Column(unique = false, nullable = false)
	private Boolean revoked;

	public Certificate() {
		// TODO Auto-generated constructor stub
	}

	public Certificate(String domainName, String serialNumber, String signatureAlgorithm, Certificate issuer,
			boolean isCA, Date validFrom, Date validTo, int issuedCertCounter, String organisation, String organisationUnit, String country) {
		super();
		this.commonName = domainName;
		this.serialNumber = serialNumber;
		this.signatureAlgorithm = signatureAlgorithm;
		this.issuer = issuer;
		this.isCA = isCA;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.issuedCertCounter = issuedCertCounter;
		this.revoked = false;
		this.organisation = organisation;
		this.organisationUnit = organisationUnit;
		this.country = country;
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String domainName) {
		this.commonName = domainName;
	}

	public int getIssuedCertCounter() {
		return issuedCertCounter;
	}

	public void setIssuedCertCounter(int issuedCertCounter) {
		this.issuedCertCounter = issuedCertCounter;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getSignatureAlgorithm() {
		return signatureAlgorithm;
	}

	public void setSignatureAlgorithm(String signatureAlgorithm) {
		this.signatureAlgorithm = signatureAlgorithm;
	}

	public Certificate getIssuer() {
		return issuer;
	}

	public void setIssuer(Certificate issuer) {
		this.issuer = issuer;
	}

	public boolean isCA() {
		return isCA;
	}

	public void setCA(boolean isCA) {
		this.isCA = isCA;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Boolean getRevoked() {
		return revoked;
	}

	public void setRevoked(Boolean revoked) {
		this.revoked = revoked;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getOrganisationUnit() {
		return organisationUnit;
	}

	public void setOrganisationUnit(String organisationUnit) {
		this.organisationUnit = organisationUnit;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
